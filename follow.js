var output = {};

(function() {
  var id = window._sharedData.entry_data.ProfilePage[0].graphql.user.id;
  var baseUrl = 'https://www.instagram.com/web/friendships/';
  var csrf_token = window._sharedData.config.csrf_token;
  var rollout_hash = window._sharedData.config.rollout_hash;

  function generateUrl() {
    var parts = [baseUrl, id, '/follow/'];
    return parts.join('');
  }

  function makeRequest(url, callback) {
    var req = new XMLHttpRequest();
    req.open('POST', url, true);
    req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    req.setRequestHeader('x-csrftoken', csrf_token);
    req.setRequestHeader('x-ig-app-id', '936619743392459');
    req.setRequestHeader('x-instagram-ajax', rollout_hash);
    req.setRequestHeader('x-requested-with', 'XMLHttpRequest');
    req.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200)
        callback(JSON.parse(this.responseText));
    };
    req.send('');
  }

  function parseResponse(data) {
    output = data;
    console.log('Status: ' + data.status);
  }

  var url = generateUrl();
  makeRequest(url, parseResponse);
})();
