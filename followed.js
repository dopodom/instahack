var list = [];

(function() {
  var username = window._sharedData.entry_data.ProfilePage[0].graphql.user.username;

  var baseUrl = 'https://www.instagram.com/graphql/query/';
  var queryHash = 'c56ee0ae1f89cdbd1c89e2bc6b8f3d18';
  var variables = {
    id: window._sharedData.entry_data.ProfilePage[0].graphql.user.id,
    include_reel: true,
    fetch_mutual: true,
    first: 50
  };

  function generateUrl(after) {
    if (after)
      variables.after = after;

    var parts = [baseUrl, '?query_hash=', queryHash, '&variables=', encodeURIComponent(JSON.stringify(variables))];

    return parts.join('');
  }

  function makeRequest(url, callback) {
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200)
        callback(JSON.parse(this.responseText));
    };
    req.open('GET', url);
    req.send();
  }

  function parseResponse(data) {
    var cc = data.data.user.edge_follow.edges;
    var hasNext = data.data.user.edge_follow.page_info.has_next_page;
    var nextHash = data.data.user.edge_follow.page_info.end_cursor;

    for (var i in cc)
      list.push(cc[i].node.username);

    if (hasNext) {
      console.log('Next url generated!');
      setTimeout(makeRequest,2000,generateUrl(nextHash), parseResponse);
    } else {
      download('followed_by_' + username + '.txt', list.join('\n'));
      console.log('No more data!');
    }

    console.log('Parsed: ' + cc.length);
    console.log('Total:' + list.length);
  }

  function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  var url = generateUrl();
  makeRequest(url, parseResponse);
})();
