#/bin/bash
#il primo parametro e' la old directory contenente i files, il secondo e' la new directory
followFile="follow_thecrochettisti.txt"
followedByFile="followed_by_thecrochettisti.txt"

function computeNotFollowing ()
{
	#computo l'intersezione tra follow e followed
    (cat $1/$followFile ;echo ""; cat $1/$followedByFile ; echo "") | sort | uniq -c | sort -n | grep "2 " | sed 's/\ *2 \(.*\)/\1/g' > $1/intersection.txt

	#prendo i not following
    (cat $1/$followedByFile ;echo ""; cat  $1/intersection.txt ;echo "") | sort | uniq -c | sort -n | grep "1 " | sed 's/\ *1 \(.*\)/\1/g' > $1/notFollowing.txt
}

#step 1 calcolo tutti quelli che non ci seguono (data vecchia)
computeNotFollowing $1

#step 1 calcolo tutti quelli che non ci seguono (data nuova)
computeNotFollowing $2

#prendo tutti gli username che non ci seguono dalla data vecchia alla data nuova
(cat $1/notFollowing.txt ;echo ""; cat $2/notFollowing.txt ;echo "";  cat toSave.txt; echo"") | sort | uniq -c | sort -n | grep "2 " | sed 's/\ *2 \(.*\)/\1/g' > tmp.txt

#followed della data nuova ed aggiungo ###davanti ai nomi che non ci seguono

sedCommand="sed  "
for i in $(cat tmp.txt)
do
   sedCommand="$sedCommand  -e s/$i/###$i/g"
done

#
# creo il file dioutput
#
$sedCommand $2/$followedByFile | grep -e "###" | sed -e "s/###\(.*\)/\1/g" > out.txt

#
# Cancello i temporanei
#
rm -f $1/intersection.txt $2/intersection.txt $1/notFollowing.txt $2/notFollowing.txt tmp.txt

